variable "hcloud_token" {
}

variable "master_count" {
}

variable "master_image" {
  description = "Predefined Image that will be used to spin up the machines (Currently supported: ubuntu-20.04, ubuntu-18.04)"
  default     = "ubuntu-20.04"
}

variable "master_type" {
  description = "For more types have a look at https://www.hetzner.de/cloud"
  default     = "cx21"
}

variable "node_count" {
}

variable "node_image" {
  description = "Predefined Image that will be used to spin up the machines (Currently supported: ubuntu-20.04, ubuntu-18.04)"
  default     = "ubuntu-20.04"
}

variable "node_type" {
  description = "For more types have a look at https://www.hetzner.de/cloud"
  default     = "cx21"
}

variable "ssh_private_key" {
  description = "Private Key to access the machines"
  default     = "~/.ssh/id_rsa"
}

variable "ssh_public_key" {
  description = "Public Key to authorized the access for the machines"
  default     = "~/.ssh/id_rsa.pub"
}

variable "docker_version" {
  default = "19.03"
}

variable "kubernetes_version" {
  default = "1.18.6"
}

variable "feature_gates" {
  description = "Add Feature Gates e.g. 'DynamicKubeletConfig=true'"
  default     = ""
}

variable "lb_type" {
  description = "Load Balancer Type"
  default     = "lb11"
}

variable "net_zone" {
  description = "Network Zone"
  default     = "eu-central"
}

variable "net_range" {
  description = "Network Range"
  default     = "10.0.0.0/16"
}

variable "subnet_range" {
  description = "SubNetwork Range"
  default     = "10.0.1.0/24"
}

variable "master_ip" {
  description = "Master Node IP"
  default     = "10.0.10.1"
}

variable "lb_ip" {
  description = "Load Balancer IP"
  default     = "10.0.1.10"
}


variable "datacenter_location" {
    description = "Hetzner Datacenter location nbg1 - Nuremberg, fsn1 - Falkenstein, hel1 - Helskinki"
    default = "nbg1"
} 
