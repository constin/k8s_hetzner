provider "hcloud" {
  token = var.hcloud_token
}

resource "hcloud_ssh_key" "k8s_admin" {
  name       = "k8s_admin"
  public_key = file(var.ssh_public_key)
}

#resource "hcloud_load_balancer" "lb1" {
#  name = "lb1"
#  load_balancer_type = var.lb_type
#  network_zone = var.net_zone
#}

#resource "hcloud_load_balancer_target" "lb1_master" {
#  type             = "server"
#  load_balancer_id = hcloud_load_balancer.lb1.id
#  server_id        = hcloud_server.master[0].id
#}

#resource "hcloud_load_balancer_target" "lb1_nodes" {
#  count            = var.node_count
#  type             = "server"
#  load_balancer_id = hcloud_load_balancer.lb1.id
#  server_id        = hcloud_server.node[count.index].id
#  use_private_ip   = true
#}

#resource "hcloud_load_balancer_service" "load_balancer_service" {
#    load_balancer_id = hcloud_load_balancer.lb1.id
#    protocol         = "http"
#    listen_port      = "80"
#    destination_port = "30080"
#}

#resource "hcloud_load_balancer_network" "k8s-net-lb" {
#  load_balancer_id = hcloud_load_balancer.lb1.id
#  network_id = hcloud_network.k8s-net.id
#  ip = var.lb_ip
#}

resource "hcloud_network" "k8s-net" {
  name = "k8s-net"
  ip_range = var.net_range
}

resource "hcloud_network_subnet" "k8s-subnet" {
  network_id = hcloud_network.k8s-net.id
  type = "cloud"
  network_zone = var.net_zone
  ip_range   = var.subnet_range
}

resource "hcloud_server_network" "k8s-net-master" {
  server_id = hcloud_server.master[0].id
  network_id = hcloud_network.k8s-net.id
  ip = var.master_ip
}

resource "hcloud_server_network" "k8s-net-node" {
  count       = var.node_count
  server_id = hcloud_server.node[count.index].id
  network_id = hcloud_network.k8s-net.id
}

resource "hcloud_server" "master" {
  count       = var.master_count
  name        = "master-${count.index + 1}"
  server_type = var.master_type
  image       = var.master_image
  ssh_keys    = [hcloud_ssh_key.k8s_admin.id]
  location    = var.datacenter_location

  connection {
    host        = self.ipv4_address
    type        = "ssh"
    private_key = file(var.ssh_private_key)
  }

  provisioner "file" {
    source      = "files/10-kubeadm.conf"
    destination = "/root/10-kubeadm.conf"
  }

  provisioner "file" {
    source      = "scripts/bootstrap.sh"
    destination = "/root/bootstrap.sh"
  }

  provisioner "remote-exec" {
    inline = ["DOCKER_VERSION=${var.docker_version} KUBERNETES_VERSION=${var.kubernetes_version} bash /root/bootstrap.sh"]
  }

  provisioner "file" {
    source      = "scripts/master.sh"
    destination = "/root/master.sh"
  }

  provisioner "remote-exec" {
    inline = ["FEATURE_GATES=${var.feature_gates} HCLOUD_API_TOKEN=${var.hcloud_token} bash /root/master.sh"]
  }

  provisioner "local-exec" {
    command = "bash scripts/copy-kubeadm-token.sh"

    environment = {
      SSH_PRIVATE_KEY = var.ssh_private_key
      SSH_USERNAME    = "root"
      SSH_HOST        = hcloud_server.master[0].ipv4_address
      TARGET          = "${path.module}/secrets/"
    }
  }
}

resource "hcloud_server" "node" {
  count       = var.node_count
  name        = "node-${count.index + 1}"
  server_type = var.node_type
  image       = var.node_image
  depends_on  = [hcloud_server.master]
  ssh_keys    = [hcloud_ssh_key.k8s_admin.id]
  location    = var.datacenter_location

  connection {
    host        = self.ipv4_address
    type        = "ssh"
    private_key = file(var.ssh_private_key)
  }

  provisioner "file" {
    source      = "files/10-kubeadm.conf"
    destination = "/root/10-kubeadm.conf"
  }

  provisioner "file" {
    source      = "scripts/bootstrap.sh"
    destination = "/root/bootstrap.sh"
  }

  provisioner "remote-exec" {
    inline = ["DOCKER_VERSION=${var.docker_version} KUBERNETES_VERSION=${var.kubernetes_version} bash /root/bootstrap.sh"]
  }

  provisioner "file" {
    source      = "${path.module}/secrets/kubeadm_join"
    destination = "/tmp/kubeadm_join"

    connection {
      host        = self.ipv4_address
      type        = "ssh"
      user        = "root"
      private_key = file(var.ssh_private_key)
    }
  }

  provisioner "file" {
    source      = "scripts/node.sh"
    destination = "/root/node.sh"
  }

  provisioner "remote-exec" {
    inline = ["bash /root/node.sh"]
  }
}

